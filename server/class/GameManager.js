import { verifyJwt } from "@hathora/server-sdk";
import { HathoraClient } from "@hathora/client-sdk";
import { AppV1Api, LobbyV2Api } from "@hathora/hathora-cloud-sdk";
import ServerGame from "./ServerGame.js";
import Player from "../../common/Player.mjs";
import { MESSAGE_TYPE_ENUM } from "../../common/Types.mjs";

class GameManager {
  server = null;
  appId = process.env.HATHORA_APP_ID;
  appSecret = process.env.HATHORA_APP_SECRET;
  devToken = process.env.DEVELOPER_TOKEN;
  appName = process.env.APP_NAME;
  encoder = new TextEncoder();
  decoder = new TextDecoder("utf-8");
  appApi = new AppV1Api();
  rooms = new Map([]);
  players = new Map([]);

  constructor() {
    if (typeof this.appId === "undefined") {
      throw new Error("HATHORA_APP_ID not set");
    }
    if (typeof this.appSecret === "undefined") {
      throw new Error("HATHORA_APP_SECRET not set!");
    }
    if (typeof this.devToken === "undefined") {
      throw new Error("DEVELOPER_TOKEN not set!");
    }
    if (typeof this.appName === "undefined") {
      throw new Error("APP_NAME not set!");
    }
    const regex = new RegExp("^[a-z0-9-]+$");
    if (!regex.test(this.appName)) {
      throw new Error("APP_NAME is not valid!");
    }

    this.lobbyClient = new LobbyV2Api();
  }

  async enableNickname() {
    const body = {
      authConfiguration: {
        nickname: {},
        anonymous: {},
      },
      appName: this.appName,
    };
    await this.appApi.updateApp(this.appId, body, {
      headers: {
        Authorization: `Bearer ${this.devToken}`,
        "Content-Type": "application/json",
      },
    });
  }

  setServer(server) {
    this.server = server;
  }

  // TODO: put user in roomId here?
  verifyToken = async (token, roomId) => {
    const userId = verifyJwt(token, process.env.HATHORA_APP_SECRET);
    if (typeof userId === "undefined") {
      // TODO: set real error here
      console.error("Failed to verify token", token);
      return;
    }

    this.updateUserData(token);

    return userId;
  };

  updateUserData(token) {
    const userdata = HathoraClient.getUserFromToken(token);
    const player = Player.createFromObj({
      id: userdata.id,
      name: userdata.name,
      gold: 99,
    });

    this.updatePlayer(player);
  }

  // subscribeUser is called when a new user enters a room
  subscribeUser = async (roomId, userId) => {
    console.log(`subscribeUser - roomId=[${roomId}] userId=[${userId}]`);

    await this.lobbyClient
      .getLobbyInfo(this.appId, roomId)
      .then((lobbyInfo) => {
        //console.log("lobbyInfo", lobbyInfo);
        const lobbyState = lobbyInfo.state ?? null;
        const lobbyInitialConfig = lobbyInfo.initialConfig ?? null;
      })
      .catch(() => {
        // WARNING: don't try to print the error here or it will crash
        console.log(`Failed to get lobbyClient.getLobbyInfo`);
      });

    // Create game if one does not exist
    if (!this.rooms.has(roomId)) {
      console.log(`subscribeUser - adding roomId=[${roomId}]`);
      this.rooms.set(
        roomId,
        new ServerGame({
          roomId: roomId,
          send: this.send,
          broadcast: this.broadcast,
        })
      );
    }

    const game = this.rooms.get(roomId);
    console.log("addPlayer", userId, this.players);
    game.addPlayer(this.players.get(userId));
    //console.log("game.players!!!!!!", game.players);
    this.broadcast(roomId, {
      type: MESSAGE_TYPE_ENUM.UPDATE_GAME,
      message: "JOIN_GAME",
      players: game.getPlayersArray(),
    });
  };

  // unsubscribeUser is called when a user disconnects from a room
  unsubscribeUser = async (roomId, userId) => {
    console.log(`unsubscribeUser - roomId=[${roomId}] userId=[${userId}].`);
    // Make sure the room exists
    if (!this.rooms.has(roomId)) {
      console.log(`unsubscribeUser - roomId=[${roomId}] does not exist.`);
      return;
    }

    const game = this.rooms.get(roomId);
    // Remove the user from the room.  If the user wasn't in the room, false is returned
    if (!game.removePlayer(userId)) {
      console.log(
        `unsubscribeUser - roomId=[${roomId}] does not contain userId=[${userId}].`
      );
      return;
    }

    this.broadcast(roomId, {
      type: MESSAGE_TYPE_ENUM.UPDATE_GAME,
      message: "UPDATE-GAME",
      players: game.getPlayersArray(),
    });

    this.server.closeConnection(
      roomId,
      userId,
      `unsubscribeUser -> closeConnection\nuserId=[${userId}] has left roomId=[${roomId}]`
    );
  };
  // onMessage is an integral part of your game's server. It is responsible for reading messages sent from the clients and handling them accordingly, this is where your game's event-based logic should live
  onMessage = async (roomId, userId, data) => {
    const message = JSON.parse(Buffer.from(data).toString("utf8"));

    // Route message to game instance via roomId
    const game = this.rooms.get(roomId);
    if (game) {
      game.deliver(userId, message);
      return;
    }

    console.log(
      `onMessage -> Unhandled message!\nroomId=[${roomId}] userId=[${userId}]`,
      message
    );
  };

  broadcast = (roomId, json) => {
    this.server.broadcastMessage(
      roomId,
      this.encoder.encode(JSON.stringify(json))
    );
  };

  send = (roomId, userId, json) => {
    this.server.sendMessage(
      roomId,
      userId,
      this.encoder.encode(JSON.stringify(json))
    );
  };

  // Tracks player userdata in a Map
  updatePlayer = (player) => {
    console.log("updatePlayer", player);
    this.players.set(player.id, player);
  };
}

export default GameManager;
