import { MESSAGE_TYPE_ENUM } from "../../common/Types.mjs";

class ServerGame {
  roomId = null;
  send = null;
  broadcast = null;

  category = null; // TODO: store settings fingerprint for the game to be able to match up snapshots?
  #players = new Map([]);
  round = 1;
  isStarted = false;
  isEnded = false;

  get players() {
    return this.#players;
  }

  constructor(params) {
    // Destructure into class variables
    ({
      roomId: this.roomId,
      receive: this.receive,
      send: this.send,
      broadcast: this.broadcast,
    } = params);
  }

  addPlayer(player) {
    this.#players.set(player.id, player);
  }

  removePlayer(userId) {
    return this.#players.delete(userId);
  }

  // Returns the players Map as an array of key value pairs for convenience
  getPlayersArray() {
    return [...this.#players.entries()].reduce(
      (obj, [key, value]) => ((obj[key] = value), obj),
      {}
    );
  }

  deliver = (userId, message) => {
    console.log("deliver", this);
    if (message.type === MESSAGE_TYPE_ENUM.GAME_TRY_START) {
      if (this.isStarted) {
        // Send error frontend
        this.send(this.roomId, userId, {
          type: MESSAGE_TYPE_ENUM.ERROR,
          message: `Game ${this.roomId} already started!`,
        });
        return;
      }
      if (this.isEnded) {
        // Send error frontend
        this.send(this.roomId, userId, {
          type: MESSAGE_TYPE_ENUM.ERROR,
          message: `Game ${this.roomId} already ended!`,
        });
        return;
      }

      this.startGame(userId, message);
      return;
    }

    if (message.type === MESSAGE_TYPE_ENUM.SHOP_TRY_END) {
      this.endShop(userId, message);
      return;
    }

    if (message.type === MESSAGE_TYPE_ENUM.BATTLE_STARTED) {
      // TODO: send battle to fronted
      this.endShop(userId, message);
      return;
    }
  };

  startGame(userId, message) {
    console.log("startGame", userId, message);
    this.isStarted = true;
    this.send(this.roomId, userId, {
      type: MESSAGE_TYPE_ENUM.GAME_STARTED,
      message: `Game ${this.roomId} started.`,
    });
  }

  endShop(userId, message) {
    console.log("endShop");
    this.send(this.roomId, userId, {
      type: MESSAGE_TYPE_ENUM.BATTLE_STARTED,
      message: "Shop ended, now what???",
    });
  }

  endBattle(userId, message) {
    console.log("endBattle");
    this.endRound();
    this.send(this.roomId, userId, {
      type: MESSAGE_TYPE_ENUM.SHOP_STARTED,
      message: "???",
    });
  }

  endRound() {
    // TODO: persist player snapshot?
    this.round++;
  }

  end() {
    // TODO: destroy game?
  }
}

export default ServerGame;
