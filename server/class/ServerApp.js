import { startServer } from "@hathora/server-sdk";
import GameManager from "./GameManager.js";

class ServerApp {
  server = null;
  gameManager = null;
  port = null;

  constructor(port) {
    this.port = port;
    if (typeof this.port === "undefined") {
      throw new Error("port not set");
    }
  }

  async start() {
    this.gameManager = new GameManager();
    this.gameManager.enableNickname();
    this.server = await startServer(this.gameManager, this.port);
    this.gameManager.setServer(this.server);
    console.log(`*----------------------------*`);
    console.log(`Server started on port ${this.port}`);
    console.log(`*----------------------------*`);
  }
}

export default ServerApp;
