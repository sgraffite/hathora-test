import { afterEach, describe, expect, it, vi } from "vitest";
import GameManager from "../class/GameManager";
import HathoraClient from "@hathora/client-sdk";
import { LobbyV2Api } from "@hathora/hathora-cloud-sdk";
import { verifyJwt } from "@hathora/server-sdk";
import Player from "../../common/Player.mjs";
import dotenv from "dotenv";
dotenv.config();

describe("#GameManager", () => {
  const mocks = vi.hoisted(() => {
    return {
      verifyJwt: vi.fn(),
      HathoraClient: class {
        static getUserFromToken(token) {
          vi.fn();
        }
      },
      Player: class {
        static createFromObj(obj) {
          vi.fn();
        }
      },
    };
  });

  vi.mock("@hathora/server-sdk", () => {
    return {
      verifyJwt: mocks.verifyJwt,
    };
  });

  vi.mock("@hathora/client-sdk", () => ({
    default: mocks.HathoraClient,
    HathoraClient: mocks.HathoraClient,
  }));

  vi.mock("../../common/Player.mjs", () => {
    return {
      default: mocks.Player,
      Player: mocks.Player,
    };
  });

  afterEach(() => {
    vi.restoreAllMocks();
  });

  it("GameManager can read env prop HATHORA_APP_ID", () => {
    vi.stubEnv("HATHORA_APP_ID", "123123");
    const gm = new GameManager({});
    expect(gm.appId).toEqual("123123");
  });

  it("GameManager can read env prop HATHORA_APP_SECRET", () => {
    vi.stubEnv("HATHORA_APP_SECRET", "234234");
    const gm = new GameManager({});
    expect(gm.appSecret).toEqual("234234");
  });

  it("GameManager can read env prop DEVELOPER_TOKEN", () => {
    vi.stubEnv("DEVELOPER_TOKEN", "345345");
    const gm = new GameManager({});
    expect(gm.devToken).toEqual("345345");
  });

  it("GameManager can read env prop APP_NAME", () => {
    vi.stubEnv("APP_NAME", "345345");
    const gm = new GameManager({});
    expect(gm.appName).toEqual("345345");
  });

  it("GameManager can setServer()", () => {
    const gm = new GameManager({});
    expect(gm.server).toEqual(null);
    gm.setServer(123);
    expect(gm.server).toEqual(123);
  });

  it("GameManager verifyToken returns userId", async () => {
    expect(verifyJwt).toBe(mocks.verifyJwt);
    vi.mocked(verifyJwt).mockReturnValue(1099);

    const gm = new GameManager({});
    const spy = vi.spyOn(gm, "updateUserData");
    spy.mockImplementationOnce(() => null);

    const generatedUserId = await gm.verifyToken("tokenValue");

    expect(spy).toHaveBeenCalledTimes(1);
    expect(generatedUserId).toBe(1099);
  });

  it("GameManager updateUserData", async () => {
    expect(HathoraClient).toBe(mocks.HathoraClient);
    expect(Player).toBe(mocks.Player);

    const spy = vi.spyOn(HathoraClient, "getUserFromToken");
    spy.mockImplementationOnce(() => {
      return { id: 999, name: "I like turtles" };
    });

    const gm = new GameManager({});
    const spy2 = vi.spyOn(gm, "updatePlayer");
    spy2.mockImplementationOnce(() => {});
    const spy3 = vi.spyOn(Player, "createFromObj");

    gm.updateUserData("tokenData");
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy2).toHaveBeenCalledTimes(1);
    expect(spy3).toHaveBeenCalledTimes(1);
  });
});
