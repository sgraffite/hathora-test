import dotenv from "dotenv";
import ServerApp from "./class/ServerApp.js";

// Load our environment variables from .env file into process.env
dotenv.config();
const port = 3000;
const serverApp = new ServerApp(port);
serverApp.start();
