class Player {
  id = null;
  name = "";
  gold = 0;

  static createFromObj(obj) {
    let player = new Player();
    Object.assign(player, obj);
    return player;
  }
}

export default Player;
