export const VISIBILITY_ENUM = Object.freeze({
  public: "public",
  private: "private",
  local: "local",
});

export const ALERT_ENUM = Object.freeze({
  success: "success",
  info: "info",
  warn: "warn",
  error: "error",
  thinking: "thinking",
});

export const MESSAGE_TYPE_ENUM = Object.freeze({
  MESSAGE: "MESSAGE",
  ERROR: "ERROR",
  UPDATE_GAME: "UPDATE_GAME",
  UPDATE_INVENTORY: "UPDATE_INVENTORY",
  GAME_TRY_START: "GAME_TRY_START",
  GAME_STARTED: "GAME_STARTED",
  SHOP_STARTED: "SHOP_STARTED",
  SHOP_TRY_END: "SHOP_TRY_END",
  BATTLE_STARTED: "BATTLE_STARTED",
  BATTLE_END: "BATTLE_END",
});
