import { Howl, Howler } from "howler";

class ShopItem {
  id = -1;
  name = null;
  moving = false;
  delta = false;
  sounds = {};

  constructor(params) {
    // Destructure into class variables
    ({ id: this.id, name: this.name } = params);
    console.log("shopitem constructor", this);

    this.sounds["itemDrop"] = new Howl({
      src: ["./sound/itemDrop.wav"],
      html5: false,
    });
  }

  dragStart = (e) => {
    //console.log("dragStart", e.target.style.zIndex);
    const x = e.clientX ? e.clientX : e.changedTouches[0].clientX;
    const y = e.clientY ? e.clientY : e.changedTouches[0].clientY;

    const bounds = e.target.getBoundingClientRect();
    this.delta = { x: x - bounds.left, y: y - bounds.top };
    //console.log("delta", this.delta);

    e.target.style.opacity = "0.6";
    this.moving = e.target;
    this.moving.style.zIndex = 999;
    this.moving.style.height = this.moving.clientHeight;
    this.moving.style.width = this.moving.clientWidth;
    this.moving.style.position = "fixed";

    // document.querySelector(".card .card-header").textContent = this.name;
    // document.querySelector("pre.card-text").textContent =
    //   this.props.item.description;

    document.querySelectorAll(`li[droppable]`).forEach((element) => {
      element.classList.add("pulse");
    });
  };

  dragMove = (e) => {
    //console.log("dragMove", e);
    const x = e.clientX >= 0 ? e.clientX : e.changedTouches[0].clientX;
    const y = e.clientY >= 0 ? e.clientY : e.changedTouches[0].clientY;
    //console.log("mousexy", { x, y });

    const newXOffset = x - this.delta.x / 2;
    const newYOffset = y - this.delta.y / 2;
    //console.log("objxy", { x: newXOffset, y: newYOffset });
    this.moving.style.left = `${newXOffset}px`;
    this.moving.style.top = `${newYOffset}px`;
  };

  dragEnd = (e) => {
    console.log("dragEnd", e);
    e.target.style.opacity = "1";

    // Reset our element
    this.moving.style.zIndex = "";
    this.moving.style.left = "";
    this.moving.style.top = "";
    this.moving.style.height = "";
    this.moving.style.width = "";
    this.moving.style.position = "";
    this.moving.style.display = "";

    const x = e.clientX ? e.clientX : e.changedTouches[0].clientX;
    const y = e.clientY ? e.clientY : e.changedTouches[0].clientY;
    this.drop(document.elementFromPoint(x, y));
    this.sounds["itemDrop"].play();

    document.querySelectorAll(`li[droppable]`).forEach((element) => {
      element.classList.remove("pulse");
    });

    this.moving = null;
  };

  drop = (target) => {
    // Dropped off the screen
    if (!target) {
      return;
    }
    target.dispatchEvent(new CustomEvent("dropItem", null));

    //this.props.dropCallback(target, this);
    //console.log("drop", target, this);
  };
}

export default ShopItem;
