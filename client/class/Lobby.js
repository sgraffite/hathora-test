import { MESSAGE_TYPE_ENUM, ALERT_ENUM } from "../../common/Types.mjs";

class Lobby {
  template = "#lobby";
  viewModel = this;

  #roomId = null;
  players = [];

  get roomId() {
    return this.#roomId;
  }
  set roomId(value) {
    this.#roomId = (value ?? "").trim();
  }
  get hasRoomId() {
    return (this.#roomId ?? "").length > 0;
  }

  constructor(params) {
    // Destructure into class variables
    ({
      app: this.app,
      roomId: this.roomId,
      userdata: this.userdata,
      sendMessage: this.sendMessage,
    } = params);
  }

  startGame = () => {
    this.sendMessage(MESSAGE_TYPE_ENUM.GAME_TRY_START, {
      asd: "I am lobby",
    }).catch((e) => {
      this.app.setAlert(ALERT_ENUM.error, e);
    });
  };

  copyGameCode(e) {
    navigator.clipboard.writeText(this.roomId);
  }

  setPlayers(players) {
    console.log("setPlayers", players);
    this.players = players;
  }
}

export default Lobby;
