import Lobby from "./Lobby";
import Shop from "./Shop";
import Battle from "./Battle";
import { MESSAGE_TYPE_ENUM, ALERT_ENUM } from "../../common/Types.mjs";

const PHASES_ENUM = Object.freeze({ LOBBY: Lobby, SHOP: Shop, BATTLE: Battle });

class Game {
  roomId = null;
  connection = null;
  token = null;
  userdata = null;
  phase = null;
  round = 0;
  players = [];
  isStarted = false;

  constructor(params) {
    // Destructure into class variables
    ({
      app: this.app,
      roomId: this.roomId,
      token: this.token,
      userdata: this.userdata,
    } = params);
  }

  start() {
    this.setPhase(PHASES_ENUM.LOBBY);
  }

  setPhase(className) {
    this.phase = new className({
      app: this.app,
      roomId: this.roomId,
      userdata: this.userdata,
      sendMessage: this.sendMessage,
    });
    console.log(
      `Phasing to [${this.phase.constructor.name}] [${this.phase.template}]`,
      this
    );
    this.template = this.phase.template;
    this.viewModel = this.phase.viewModel;
    this.app.route(this);
  }

  async setConnection(connection) {
    console.log("userdata", this.userdata);
    this.userdata.token = this.token;
    return new Promise((resolve, reject) => {
      this.connection = connection;
      this.connection.onMessageJson(this.getJSONmsg);
      this.connection.onClose(this.onClose);
      this.connection
        .connect(this.token)
        // .then(() =>
        //   // TODO: send avatar?
        //   this.sendMessage(MESSAGE_TYPE_ENUM.CONNECTED, this.userdata)
        // )
        .then(() => {
          resolve();
        })
        .catch((e) => {
          reject(`Failed to connect to server! ${e}`);
        });
    });
  }

  async leaveRoom() {
    if (!this.parent.client) {
      return;
    }

    this.parent.client.disconnect();
  }

  getJSONmsg = (message) => {
    // Game started, route to shop
    if (message.type === MESSAGE_TYPE_ENUM.ERROR) {
      console.log("message", message);
      this.app.setAlert(ALERT_ENUM.error, message.message);
      return;
    }

    // Game started, route to shop
    if (message.type === MESSAGE_TYPE_ENUM.GAME_STARTED) {
      this.isStarted = true;
      this.setPhase(PHASES_ENUM.SHOP);
      this.app.setAlert(ALERT_ENUM.success, message.message, 5000);
      return;
    }

    // Battle started, route to battle
    if (message.type === MESSAGE_TYPE_ENUM.BATTLE_STARTED) {
      this.setPhase(PHASES_ENUM.BATTLE);
      return;
    }

    // Update players in the lobby
    if (message.type === MESSAGE_TYPE_ENUM.UPDATE_GAME) {
      if (this.phase.constructor.name === "Lobby") {
        if (message.players) {
          console.log("this.players", message.players);
          this.phase.setPlayers(Object.values(message.players));
        }
      }

      return;
    }

    console.log("Unhandled getJSONmsg!", message);
  };

  sendMessage = async (type, data) => {
    return this.connection?.writeJson({
      type: type,
      data: data,
    });
  };

  onClose = (e) => {
    this.app.setAlert(ALERT_ENUM.error, "Connection to server lost!");
    this.app.route();
  };
}

export default Game;
