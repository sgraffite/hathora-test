import { LobbyV2Api, RoomV2Api, Region } from "@hathora/hathora-cloud-sdk";
import { HathoraConnection } from "@hathora/client-sdk";
import Game from "./Game";
import { VISIBILITY_ENUM, ALERT_ENUM } from "../../common/Types.mjs";

export const LOCAL_CONNECTION_DETAILS = {
  host: "localhost",
  port: 3000,
  transportType: "tcp",
};

class Main {
  template = "#main";
  viewModel = this;

  appId = null;
  token = null;
  roomClient = new RoomV2Api();
  lobbyClient = new LobbyV2Api();
  localOnly = true;

  #roomId = null;
  animations = [];

  get roomId() {
    return this.#roomId;
  }
  set roomId(value) {
    this.#roomId = (value ?? "").trim();
  }
  get hasRoomId() {
    return (this.#roomId ?? "").length > 0;
  }
  get canJoinGame() {
    return this.hasRoomId && !this.app.isWorking;
  }

  constructor(params) {
    // Destructure into class variables
    ({
      app: this.app,
      appId: this.appId,
      token: this.token,
      userdata: this.userdata,
    } = params);

    if (!this.token) {
      throw new Error("Main - [player token] not set!");
    }
    if (!this.appId) {
      throw new Error("Main - [appId] not set!");
    }
  }

  random(min, max) {
    return Math.random() * (max - min) + min;
  }
  randomInt(min, max) {
    return Math.floor(this.random(min, max));
  }
  clicked(e) {
    const animation = {
      value: this.randomInt(-15, -1),
      x: Math.min(Math.max(e.offsetX, 50), 590),
      y: Math.min(Math.max(e.offsetY, 50), 430),
    };
    this.animations.push(animation);
    UI.queue(
      () => (this.animations = this.animations.filter((a) => a !== animation))
    );
  }

  async createGame() {
    if (this.app.working) {
      return;
    }
    this.working = true;
    const visibility = "local";
    const region = "Chicago";
    const config = {};
    const roomConfig = {
      state: {},
      visibility: VISIBILITY_ENUM.hasOwnProperty(visibility)
        ? visibility
        : VISIBILITY_ENUM.local,
      region: region,
      initialConfig: config,
    };
    // For now roomIds are unique per applicaton for all time
    // So I am leaving out the optional [roomId] param and not accepting user input for roomId
    await this.app
      .setAlert(ALERT_ENUM.thinking, `Creating game`)
      .then(() =>
        this.lobbyClient.createLobby(this.appId, this.token, roomConfig)
      )
      .then((roomInfo) => {
        this.roomId = roomInfo.roomId;
      })
      .then(() => this.joinGame())
      .then(() => this.app.closeAlert())
      .catch((e) =>
        this.app.setAlert(
          ALERT_ENUM.error,
          `Error joining game ${this.roomId}! ${e}`
        )
      );
  }

  async getConnectionInfo(roomId) {
    //console.log(`getConnectionInfo:\nappId=${this.appId}\nroomId=${roomId}`);
    let connectionInfo = LOCAL_CONNECTION_DETAILS;
    if (this.localOnly) {
      return connectionInfo;
    }

    console.log("localOnly", this.localOnly);
    await this.roomClient
      .getConnectionInfo(this.appId, roomId)
      .then((result) => {
        connectionInfo = result.exposedPort;
        return connectionInfo;
      })
      .catch(() => {
        reject(`Failed to get connectionInfo!`);
      });
  }

  joinGame() {
    const roomId = this.roomId;
    if (!roomId) {
      this.app.setAlert(
        ALERT_ENUM.error,
        `Error joining game, must provide roomId!`
      );
      return;
    }

    const game = new Game({
      app: this.app,
      roomId: roomId,
      token: this.token,
      userdata: this.userdata,
    });
    return this.getConnectionInfo(roomId)
      .then((connectionInfo) => new HathoraConnection(roomId, connectionInfo))
      .then((connection) => game.setConnection(connection))
      .then(() => game.start())
      .catch((e) => {
        this.app.setAlert(ALERT_ENUM.error, `Failed to connect to server!`);
      });
  }

  async getPublicGames() {
    if (!this.localOnly) {
      return await this.lobbyClient.listActivePublicLobbies(this.appId);
    }
  }

  setRoomId(room) {
    if (typeof this.roomId !== "undefined") {
      return;
    }

    this.roomId = room;
  }
}

export default Main;
