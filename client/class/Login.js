import { HathoraClient } from "@hathora/client-sdk";
import { AuthV1Api } from "@hathora/hathora-cloud-sdk";

class Login {
  template = "#login";
  viewModel = this;

  #nickname = null;
  cachedToken;

  get nickname() {
    return this.#nickname;
  }
  set nickname(value) {
    this.#nickname = (value ?? "").trim();
  }
  get hasNickname() {
    return (this.#nickname ?? "").length > 0;
  }

  constructor(params) {
    // Destructure into class variables
    ({ appId: this.appId, setUserData: this.setUserData } = params);

    this.cachedToken = sessionStorage.getItem("token");
    this.nickname = sessionStorage.getItem("nickname");
    if (this.cachedToken && this.nickname) {
      this.login();
    }
  }

  async login() {
    //this.token = await this.getAnonymousToken(nickname);
    this.#getAnonymousToken(this.nickname)
      .then((token) => {
        const userdata = HathoraClient.getUserFromToken(token);
        sessionStorage.setItem("nickname", this.nickname);
        //console.log("User Token: ", this.token);
        console.log("UserId: ", userdata.id);
        console.log("User type: ", userdata.type);
        console.log("Username: ", userdata.name);

        this.setUserData(token, userdata);
      })
      .catch((e) => {
        console.log("Failed to login!", e);
      });
  }

  async #getAnonymousToken(nickname) {
    if (this.cachedToken) {
      return this.cachedToken;
    }

    const authClient = new AuthV1Api();
    console.log("nickname", nickname);
    const reponse =
      true && nickname
        ? await authClient.loginNickname(this.appId, { nickname })
        : await authClient.loginAnonymous(this.appId);
    sessionStorage.setItem("token", reponse.token);
    return reponse.token;
  }
}

export default Login;
