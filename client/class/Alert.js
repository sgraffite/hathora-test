import {ALERT_ENUM } from "../../common/Types.mjs";

class Alert {
  type = null;
  message = "";
  timeoutDelay = 0; // Milliseconds
  isVisible = false;
  timeout = null;
  
  get isThinking() {
    return this.type === ALERT_ENUM.thinking;
  }

  update = (params) => {
    // Destructure into class variables
    ({
      type: this.type,
      message: this.message,
      timeoutDelay: this.timeoutDelay,
    } = params);

    if (this.timeoutDelay) {
      this.timeout = setTimeout(() => {
        this.close();
      }, this.timeoutDelay);
    }

    this.isVisible = true;
  };

  close = () => {
    this.isVisible = false;
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  };
}

export default Alert;
