import ShopItem from "./ShopItem";
import { MESSAGE_TYPE_ENUM } from "../../common/Types.mjs";

const items = [
  {
    id: 1,
    name: "Dagger",
    description: "+1 attack",
    spriteId: "dagger",
  },
  {
    id: 2,
    name: "Leather Boots",
    description: "+1 defense",
    spriteId: "leather-boots",
  },
  {
    id: 3,
    name: "Chain Armor",
    description: "+4 defense",
    spriteId: "chain-armor",
  },
];

class Shop {
  template = "#shop";
  viewModel = this;

  inventorySlots = [1, 2, 3, 4];
  shopItems = [];
  lastMovedShopItem = null;

  constructor(params) {
    // Destructure into class variables
    ({ sendMessage: this.sendMessage, app: this.app } = params);

    items.forEach((itemData) => {
      const params = itemData;
      params.app = this.app;
      this.shopItems.push(new ShopItem(params));
    });
  }

  cancelMe(e) {
    console.log("shopItem dragOver/Enter", e);
    e.preventDefault();
  }

  drop = (e) => {
    // TODO: route to proper bucket
    console.log("shopItem drop", e);
  };

  getItemByTarget(e) {
    const shopItemId = Number(e.target.getAttribute("data-id"));
    return this.shopItems.find((shopItem) => shopItem.id === shopItemId);
  }

  dragStart = (e) => {
    const shopItem = this.getItemByTarget(e);
    shopItem && shopItem.dragStart(e);
  };

  dragMove = (e) => {
    const shopItem = this.getItemByTarget(e);
    shopItem && shopItem.moving && shopItem.dragMove(e);
  };

  dragEnd = (e) => {
    const shopItem = this.getItemByTarget(e);
    shopItem && shopItem.moving && shopItem.dragEnd(e);
  };

  end() {
    this.sendMessage(MESSAGE_TYPE_ENUM.SHOP_TRY_END, {
      asd: "I am shop",
    });
  }
}

export default Shop;
