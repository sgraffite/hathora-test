import { UI } from "https://cdn.skypack.dev/@peasy-lib/peasy-ui";
import Main from "./Main";
import Alert from "./Alert";
import Login from "./Login";

class App {
  appId = null;
  token = null;
  userdata = null;
  roomId = null; // GameId
  view = null;
  #alert = null;
  working = false;

  constructor(appId) {
    this.appId = appId;
    this.#alert = new Alert();
    UI.create(document.querySelector("#alert"), this.#alert, "#alert-template");
    UI.create(document.querySelector("#app"), this, "#footer-template");
    //UI.initialize(2); // Limit to 60hz
    this.route();
  }

  setAlert = (type, message, timeoutDelay) => {
    return new Promise((resolve, reject) => {
      this.#alert.update({
        type: type,
        message: message,
        timeoutDelay: timeoutDelay,
      });
      resolve();
    });
  };

  closeAlert = () => {
    this.#alert.close();
  };

  route = async (instance) => {
    if (!this.token) {
      instance = new Login({
        app: this,
        appId: this.appId,
        setUserData: this.setUserData,
      });
    }

    if (!instance) {
      instance = new Main({
        app: this,
        appId: this.appId,
        token: this.token,
        userdata: this.userdata,
      });
    }

    await this.#router(instance);
  };

  // TODO: make player object?
  setUserData = (token, userData) => {
    this.token = token;
    this.userdata = userData;
    this.route();
  };

  logout = () => {
    console.log("logout", this.token);
    sessionStorage.clear("nickname");
    sessionStorage.clear("token");
    this.token = null;
    this.route();
  };

  async #router(instance) {
    console.log(
      `Routing to [${instance.constructor.name}] [${instance.template}]`
    );

    if (this.view) {
      this.view.destroy();
      await this.view.detached;
    }

    this.view = UI.create(
      document.querySelector("#app"),
      instance.viewModel,
      instance.template
    );
    await this.view.attached;
    this.working = false;
  }
}

export default App;
