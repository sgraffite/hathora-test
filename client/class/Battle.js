import { MESSAGE_TYPE_ENUM } from "../../common/Types.mjs";

class Battle {
  template = "#battle";
  viewModel = this;

  constructor(params) {
    // Destructure into class variables
    ({ sendMessage: this.sendMessage } = params);
  }

  end() {
    this.sendMessage(MESSAGE_TYPE_ENUM.BATTLE_END, {
      asd: "I am battle",
    });
  }
}

export default Battle;
